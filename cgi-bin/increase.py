#!/usr/bin/env python
# -*- coding: utf-8 -*-
import cgi
import sys
import html
import http.cookies
import os

def increase():
    get = open('index.html', 'r', encoding='utf-8')
    cur_line = ""

    content = ""

    for i in range(31):
        cur_line = get.readline()
        sys.stderr.write(cur_line + '\n')
        content += cur_line
    cur_line = get.readline()
    lst = cur_line.split(' ')
    sys.stderr.write(str(lst) + '\n')
    counter = int(lst[11]) + 1
    for i in range(11):
        content += lst[i]
        content += ' '
    content += str(counter) + ' '
    content += lst[12]
    # content += '{0} {1} {2} {3} {4}'.format(lst[0], lst[1], lst[2], str(counter), lst[4])
    for i in range(2):
        global cur_line
        cur_line = get.readline()
        content += cur_line

    get.close()

    put = open('index.html', 'w', encoding='utf-8')
    put.write(content)
    put.close()

cookie = http.cookies.SimpleCookie(os.environ.get("HTTP_COOKIE"))
name = cookie.get("used")


def enc_print(string='', encoding='utf8'):
    sys.stdout.buffer.write(string.encode(encoding) + b'\n')


if name is None:
    increase()
    enc_print("Set-cookie: used=true; expires=Nov 26 02:28:00 2022; path=/cgi-bin/")
    enc_print("Content-type: text/html\n")
    enc_print("""<!DOCTYPE HTML>
            <html>
            <head>
                <meta charset="utf-8">
                <meta http-equiv="refresh" content="5; url=../index.html" />
                <title>Formy</title>
                <style>
                h1 {
                    font-family: Consolas, monospace;
                    color: #0F0;
                }
                </style>
            </head>
            <body bgcolor="black">""")

    enc_print("<h1>Дзякуй, зараз скіруем вас на галоўную.</h1>")
    enc_print("""</body>
            </html>""")
else:
    enc_print("Content-type: text/html\n")
    enc_print("""<!DOCTYPE HTML>
                <html>
                <head>
                    <meta charset="utf-8">
                    <meta http-equiv="refresh" content="5; url=../index.html" />
                    <title>Formy</title>
                    <style>
                    h1 {
                        font-family: Consolas, monospace;
                        color: #F00;
                    }
                    </style>
                </head>
                <body bgcolor="black">""")

    enc_print("<h1>Нам ужо вядома, дзякуй.</h1>")
    enc_print("""</body>
                </html>""")